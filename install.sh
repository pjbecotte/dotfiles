
apt-get update
apt-get install -y gnupg software-properties-common curl

# Python
add-apt-repository -y ppa:deadsnakes/ppa
apt-get install -y python3.9-dev python3.9-venv python3.9-distutils libffi-dev libpq-dev
python3.9 -m ensurepip


# HELM
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

# Kubectl
curl -L "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" -o /tmp/kubectl
install -o root -g root -m 0755 /tmp/kubectl /usr/local/bin/kubectl
rm /tmp/kubectl

# Terraform
curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
apt-get install -y terraform

# doctl
curl -L "https://github.com/digitalocean/doctl/releases/download/v1.62.0/doctl-1.62.0-linux-amd64.tar.gz" -o /tmp/doctl.tar.gz
tar xf /tmp/doctl.tar.gz -C /tmp
mv /tmp/doctl /usr/local/bin/doctl
rm /tmp/doctl
rm /tmp/doctl.tar.gz

cat .bashrc >> ~/.bashrc
cp .gitconfig ~/.gitconfig
echo "doctl kubernetes cluster kubeconfig save pbecotte" >> ~/.bash_profile
