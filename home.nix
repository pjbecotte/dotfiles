{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "pbecotte";
  home.homeDirectory = "/home/pbecotte";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.05"; # Please read the comment before changing.

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = [
    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')

    pkgs.curl
    pkgs.gnugrep
    pkgs.jq
    pkgs.podman
    pkgs.vim
    pkgs.docker
    pkgs.rootlesskit
    pkgs.slirp4netns
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    ".gitconfig".text = ''
      [user]
        name = Paul Becotte
        email = pjbecotte@hey.com
      [alias]
        co = checkout
        br = branch
        ci = commit
        st = status
      [color]
        branch = auto
        diff = auto
        status = auto
      [color "branch"]
        current = yellow reverse
        local = yellow
        remote = green
      [color "diff"]
        meta = yellow bold
        frag = magenta bold
        old = red bold
        new = green bold
      [color "status"]
        added = yellow
        changed = green
        untracked = cyan
    '';

  };

  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  home.sessionVariables = {
    # EDITOR = "emacs";
    DOCKER_HOST = "unix:///run/user/1000/docker.sock";
  };

  programs = {
    home-manager.enable = true;
    git = {
      package = pkgs.gitAndTools.gitFull;
      enable = true;
    };
    bash = {
      enable = true;
      enableCompletion = true;
      shellAliases = {
        ll = "ls -alF";
        k = "kubectl";
        tf = "terraform";
      };
      initExtra = ''
        YELLOW="\[\e[1;33m\]"
        CYAN="\[\e[0;36m\]"
        COLOR_NONE="\[\e[0m\]"

        source ~/.nix-profile/share/git/contrib/completion/git-prompt.sh
        GIT_PS1_SHOWDIRTYSTATE=true
        PS1="''${PYTHON_VIRTUALENV}''${YELLOW}\w''${CYAN}\$(__git_ps1)\$ ''${COLOR_NONE}"
        source ''${HOME}/.secrets
      '';
    };
    direnv = {
      enable = true;
      enableBashIntegration = true;
      nix-direnv.enable = true;
    };
  };
  targets.genericLinux.enable = true;

  systemd.user.services.docker = {
    Unit = {
      Description = "Docker Application Container Engine (Rootless)";
    };
    Service = {
      Restart = "always";
      RestartSec = 2;
      StartLimitBurst = 3;
      StartLimitInterval = "60s";
      LimitNOFILE = "infinity";
      LimitNPROC = "infinity";
      LimitCORE = "infinity";
      TasksMax = "infinity";
      Delegate = "yes";
      Type = "notify";
      NotifyAccess = "all";
      KillMode = "mixed";
      ExecStart = "${pkgs.docker}/bin/dockerd-rootless";
      Environment = "PATH=${config.home.profileDirectory}/bin:/usr/bin";
    };
    Install = {
      WantedBy = [ "default.target" ];
    };
  };
}
