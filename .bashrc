YELLOW="\e[1;33m"
CYAN="\e[0;36m"
COLOR_NONE="\e[0m"

export GIT_PS1_SHOWDIRTYSTATE=true
PS1="${PYTHON_VIRTUALENV}${YELLOW}\w${CYAN}\$(__git_ps1)\$ ${COLOR_NONE}"
export PATH=~/.cargo.bin:/usr/local/go/bin:~/.local/bin:$PATH
source <(kubectl completion bash)
alias k=kubectl
source <(kubectl completion bash | sed 's|__start_kubectl kubectl|__start_kubectl k|g')
alias tf=terraform

# Variables for devblog development
export TF_VAR_DO_TOKEN=
export TF_VAR_GITLAB_TOKEN=
# These are fake aws keys- they are for the DO space bucket that TF uses as a backend
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
# These are the real aws keys- they get stored in kube secrets for the app to use
export TF_VAR_AWS_ACCESS_KEY_ID=
export TF_VAR_AWS_SECRET_ACCESS_KEY=
